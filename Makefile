#VAR
OUT = compiler
LEXFLAG = -ll
MAKEYACC = yacc -y -d
MAKELEX = flex
DIR = _build

#ALL
all : $(DIR) $(DIR)/yacc.o $(DIR)/lex.o $(DIR)/table.o $(DIR)/instructionTable.o
	gcc $(DIR)/yacc.o $(DIR)/lex.o $(DIR)/table.o $(DIR)/instructionTable.o -o $(OUT) $(LEXFLAG)

#YACC RELATED
$(DIR)/yacc.o : $(DIR)/y.tab.c
	gcc -c $^ -o $@

$(DIR)/y.tab.c : src/compiler.y
	$(MAKEYACC) $^
	mv y.tab.c $@
	mv y.tab.h $(DIR)/y.tab.h

#LEX RELATED
$(DIR)/lex.o : $(DIR)/lex.yy.c
	gcc -c $^ -o $@

$(DIR)/lex.yy.c : src/compiler.l
	$(MAKELEX) $^
	mv lex.yy.c $@

#TABLE RELATED
$(DIR)/table.o : src/table.c src/table.h
	gcc -c $< -o $@

#INSTRUCTION TABLE RELATED
$(DIR)/instructionTable.o : src/instructionTable.c src/instructionTable.h
	gcc -c $< -o $@

#CLEAN
clean : 
	rm -rf $(DIR)/*.o
	rm -rf $(DIR)/*.c
	rm -rf $(DIR)/*.h

#CLEANER
cleaner : clean
	rm -rf $(OUT)

#MKDIR
$(DIR) :
	mkdir -p $(DIR) 

