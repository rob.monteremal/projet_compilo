----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:09:27 04/18/2019 
-- Design Name: 
-- Module Name:    ALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           B : in  STD_LOGIC_VECTOR (7 downto 0);
           OP : in  STD_LOGIC_VECTOR (7 downto 0);
           S : out  STD_LOGIC_VECTOR (7 downto 0);
           Flags : out  STD_LOGIC_VECTOR (3 downto 0));
end ALU;

architecture Behavioral of ALU is

Signal S_add : STD_LOGIC_VECTOR (11 downto 0) := X"000";
Signal S_mul : STD_LOGIC_VECTOR (15 downto 0) := X"0000";
Signal S_sub : STD_LOGIC_VECTOR (11 downto 0) := X"000";
Signal S_div : STD_LOGIC_VECTOR (15 downto 0) := X"0000";
Signal S_equ : STD_LOGIC_VECTOR (7 downto 0) := X"00";
Signal S_inf : STD_LOGIC_VECTOR (7 downto 0) := X"00";
Signal S_infe : STD_LOGIC_VECTOR (7 downto 0) := X"00";
Signal S_sup : STD_LOGIC_VECTOR (7 downto 0) := X"00";
Signal S_supe : STD_LOGIC_VECTOR (7 downto 0) := X"00";
Signal S_tmp : STD_LOGIC_VECTOR (7 downto 0) := X"00";

begin

-- operation
S_add <= (X"0"&A) + (X"0"&B);	-- addition
S_mul <= A*B;	-- multiplication
S_sub <= (X"0"&A) - (X"0"&B);	-- substraction

S_equ <= X"01" when A=B else X"00";
S_inf <= X"01" when A<B else X"00";
S_infe <= X"01" when A<=B else X"00";
S_sup <= X"01" when A>B else X"00";
S_supe <= X"01" when A>=B else X"00";

-- results
S_tmp <= S_add(7 downto 0) when OP=X"91" else
S_mul(7 downto 0) when OP=X"92" else
S_sub(7 downto 0) when OP=X"93" else
S_equ(7 downto 0) when OP=X"97" else
S_inf(7 downto 0) when OP=X"98" else
S_infe(7 downto 0) when OP=X"99" else
S_sup(7 downto 0) when OP=X"9A" else
S_supe(7 downto 0) when OP=X"9B";

-- flags
-- C : output has Carry
Flags(3) <= S_add(8) when OP=X"91" else
S_sub(8) when OP=X"93" else '0';

-- N : output is Negative
Flags(2) <= S_tmp(7);

-- Z : output is Zero
Flags(1) <= '1' when S_tmp=X"00" else '0';

-- O : value Overflowed
Flags(0) <= '1' when 
	((((A(7)='0' and B(7)='0' and S_tmp(7)='1') or
	(A(7)='1' and B(7)='1' and S_tmp(7)='0')) and OP=X"91")
	or
	(((A(7)='1' and B(7)='0' and S_tmp(7)='0') or
	(A(7)='0' and B(7)='1' and S_tmp(7)='1')) and OP=X"93")
	or
	((S_mul(15 downto 8) /=X"00") and (OP=X"92"))) else '0';


S <= S_tmp;

end Behavioral;