----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:06:48 05/17/2019 
-- Design Name: 
-- Module Name:    PROCESSEUR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BANC_INSTRUCTION is
    Port (
			 CK : in std_logic;
			 ins_a: in std_logic_vector(7 downto 0);
			 ins_di: out std_logic_vector(31 downto 0)
			 );
end BANC_INSTRUCTION;

architecture Structural of BANC_INSTRUCTION is

	 COMPONENT instr_memory
    PORT(
         sel : in  std_logic_vector(7 downto 0);
         q : out  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
	 
begin
	bcInst: instr_memory port map(ins_a, ins_di);

end Structural;
