----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:12:44 05/29/2019 
-- Design Name: 
-- Module Name:    IP - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IP is
    Port ( CK : in  STD_LOGIC;
			  jmp : in STD_LOGIC;
			  jmp_a : in STD_LOGIC_VECTOR(7 downto 0);
           counter : out  STD_LOGIC_VECTOR(7 downto 0)
			 );
end IP;

architecture Behavioral of IP is

	signal c: unsigned(7 downto 0) := X"00";
begin
	process
	begin
		wait until rising_edge(CK);
		c <= c + 1;
		if jmp='1' then
			c <= unsigned(jmp_a);
			counter <= jmp_a;
		else
			counter <= std_logic_vector(c);
		end if;
	end process;

end Behavioral;

