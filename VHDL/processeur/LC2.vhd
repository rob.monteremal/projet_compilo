----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:28:42 05/27/2019 
-- Design Name: 
-- Module Name:    LC2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LC2 is
    Port ( OP_in : in  STD_LOGIC_VECTOR (7 downto 0);
           OP_out : out  STD_LOGIC
			 );
end LC2;

architecture Behavioral of LC2 is

begin

OP_out <= '0' when OP_in = X"36" else '1';

end Behavioral;

