--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:32:33 05/27/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/LC2_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: LC2
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY LC2_TEST IS
END LC2_TEST;
 
ARCHITECTURE behavior OF LC2_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT LC2
    PORT(
         OP_in : IN  std_logic_vector(7 downto 0);
         OP_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal OP_in : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal OP_out : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: LC2 PORT MAP (
          OP_in => OP_in,
          OP_out => OP_out
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

		OP_in <= X"00", X"36" after 100 ns, X"00" after 200 ns;

      -- insert stimulus here 

      wait;
   end process;

END;
