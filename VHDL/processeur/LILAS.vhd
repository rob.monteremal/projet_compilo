----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:06:05 05/20/2019 
-- Design Name: 
-- Module Name:    LILAS - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LILAS is
    Port ( CK : in  STD_LOGIC;
			  input_ins_8 : in std_logic_vector(7 downto 0);
           dummyOutput : out  STD_LOGIC_VECTOR(7 downto 0)
			 );
end LILAS;

architecture Structural of LILAS is

    COMPONENT PROCESSEUR
    PORT(
         CK : IN  std_logic;
			RST_REG: IN std_logic;
         ins_di : IN  std_logic_vector(31 downto 0);
         data_di : IN  std_logic_vector(7 downto 0);
         data_we : OUT  std_logic;
         data_a : OUT  std_logic_vector(7 downto 0);
         data_do : OUT  std_logic_vector(7 downto 0);
		   jmp: out std_logic;
		   jmp_a: out std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	
	COMPONENT instr_memory
    PORT(
         sel : in  std_logic_vector(7 downto 0);
         q : out  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
	 
    COMPONENT IP
    PORT(
         CK : IN  std_logic;
         jmp : IN  std_logic;
         jmp_a : IN  std_logic_vector(7 downto 0);
         counter : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT banc_memoire
    PORT(
         CK : IN  std_logic;
         RST : IN  std_logic;
         DATAIN : IN  std_logic_vector(7 downto 0);
         at : IN  std_logic_vector(7 downto 0);
         RW : IN  std_logic;
         DATAOUT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT splitter
    PORT(
         in_pipe : IN  std_logic_vector(7 downto 0);
         out_bcr : OUT  std_logic_vector(7 downto 0);
         out_proc : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;


	 signal ins_di : std_logic_vector(31 downto 0);
	 signal splitter_mem2cpu: std_logic_vector(7 downto 0);	-- cable du splitter
	 signal data_di, data_a, data_do : std_logic_vector(7 downto 0);
	 signal data_we: std_logic;
	 signal select_ins, jmp_a : std_logic_vector(7 downto 0); -- cable entre IP et banc instruction
	 signal jmp: std_logic;
	
begin

	cpu: PROCESSEUR port map(CK, '1', ins_di, data_di, data_we, data_a, data_do, jmp, jmp_a); 
	
	 --CK : in std_logic;
	--		 ins_di: in std_logic_vector(31 downto 0);
	--		 data_di: in std_logic_vector(7 downto 0);
	--		 data_we: out std_logic;
	--		 data_a: out std_logic_vector(7 downto 0);
	--		 data_do: out std_logic_vector(7 downto 0)
	--		);
			
	ipp: IP port map(CK, jmp, jmp_a, select_ins);		
	inst: instr_memory port map(select_ins, ins_di);
	mem: banc_memoire port map(CK, '1', data_do, data_a, data_we, splitter_mem2cpu);
	spliter_output: splitter port map(splitter_mem2cpu, data_di, dummyOutput);
	-- mem2: banc_memoire port map(CK, '1', X"00", X"00", '0', dummyOutput); 

   -- ( CK : in STD_LOGIC;
	--		  RST : in STD_LOGIC;
		--	  DATAIN : in  STD_LOGIC_VECTOR (7 downto 0);
        --   at : in  STD_LOGIC_VECTOR (7 downto 0);
          -- RW : in  STD_LOGIC;
           --DATAOUT : out  STD_LOGIC_VECTOR (7 downto 0));
			  
end Structural;

