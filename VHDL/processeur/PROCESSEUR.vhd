----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:06:48 05/17/2019 
-- Design Name: 
-- Module Name:    PROCESSEUR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PROCESSEUR is
    Port (
			 CK : in std_logic;
			 RST_REG: in std_logic;
			 ins_di: in std_logic_vector(31 downto 0);
			 data_di: in std_logic_vector(7 downto 0);
			 data_we: out std_logic;
			 data_a: out std_logic_vector(7 downto 0);
			 data_do: out std_logic_vector(7 downto 0);
			 jmp: out std_logic;
			 jmp_a: out std_logic_vector(7 downto 0)
			);
end PROCESSEUR;

architecture Structural of PROCESSEUR is

    COMPONENT JMP_LOGIC
    PORT(
         OP : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);
         jmp : OUT  std_logic
        );
    END COMPONENT;
	 
    COMPONENT banc_memoire
    PORT(
         CK : IN  std_logic;
         RST : IN  std_logic;
         DATAIN : IN  std_logic_vector(7 downto 0);
         at : IN  std_logic_vector(7 downto 0);
         RW : IN  std_logic;
         DATAOUT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT banc_registre
    PORT(
         CK : IN  std_logic;
         RST : IN  std_logic;
         atA : IN  std_logic_vector(3 downto 0);
         atB : IN  std_logic_vector(3 downto 0);
         W : IN  std_logic;
         atW : IN  std_logic_vector(3 downto 0);
         DATA : IN  std_logic_vector(7 downto 0);
         QA : OUT  std_logic_vector(7 downto 0);
         QB : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT ALU
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         B : IN  std_logic_vector(7 downto 0);
         OP : IN  std_logic_vector(7 downto 0);
         S : OUT  std_logic_vector(7 downto 0);
         Flags : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT decodeur
    PORT(
         INST : IN  std_logic_vector(31 downto 0);
         A : OUT  std_logic_vector(7 downto 0);
         B : OUT  std_logic_vector(7 downto 0);
         C : OUT  std_logic_vector(7 downto 0);
         OP : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT pipeline
    PORT(
         CK : IN  std_logic;
         A_in : IN  std_logic_vector(7 downto 0);
         B_in : IN  std_logic_vector(7 downto 0);
         C_in : IN  std_logic_vector(7 downto 0);
         OP_in : IN  std_logic_vector(7 downto 0);
         A_out : OUT  std_logic_vector(7 downto 0);
         B_out : OUT  std_logic_vector(7 downto 0);
         C_out : OUT  std_logic_vector(7 downto 0);
         OP_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT instr_memory
    PORT(
         sel : IN  std_logic_vector(15 downto 0);
         q : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT LC
    PORT(
         OP_in : IN  std_logic_vector(7 downto 0);
         OP_out : OUT  std_logic
        );
    END COMPONENT;
	 
	  COMPONENT LC2
    PORT(
         OP_in : IN  std_logic_vector(7 downto 0);
         OP_out : OUT  std_logic
        );
    END COMPONENT;
	 
	 COMPONENT splitter
    PORT(
         in_pipe : IN  std_logic_vector(7 downto 0);
         out_bcr : OUT  std_logic_vector(7 downto 0);
         out_proc : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 COMPONENT multiplexeur
	 GENERIC( pos : NATURAL);
    PORT(
         OP : IN  std_logic_vector(7 downto 0);
         B_in : IN  std_logic_vector(7 downto 0);
         QA : IN  std_logic_vector(7 downto 0);
         B_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
	 
	 
	 signal dec_OP, DI_OP, EX_OP, Mem_OP, RE_OP,					-- signaux pour OP
			  dec_A, DI_A, EX_A, Mem_A, RE_A,						-- signaux pour A
			  dec_B, DI_B, EX_B, Mem_B, RE_B, splitMEM_B,      -- signaux pour B
			  dec_C, DI_C, EX_C, Mem_C,								-- signaux pour C
			  REG_QB,														-- sortie QB -> DI/EX[B] du banc de registre
			  muxREG_B, muxREG_QA,										-- signaux pour le multiplexeur entre LI/DI et DI/EX
			  muxALU_B, muxALU_S,										-- signaux pour le multiplexeur entre DI/EX et EX/Mem
	        muxRE_DATA,													-- signaux pour le multiplexeur apres Mem/RE
			  DI_A_split
			  : std_logic_vector(7 downto 0);
	 signal lc_out : std_logic;
	 
begin
	DEC: decodeur port map(ins_di, dec_A, dec_B, dec_C, dec_OP);
	LIDI: pipeline port map(CK, dec_A, dec_B, dec_C, dec_OP, DI_A, DI_B, DI_C, DI_OP);
	DIEX: pipeline port map(CK, DI_A_split, muxREG_B, REG_QB, DI_OP, EX_A, EX_B, EX_C, EX_OP);
   EXMem: pipeline port map(CK, EX_A, muxALU_B, X"00", EX_OP, Mem_A, Mem_B, open, Mem_OP);
	MemRE: pipeline port map(CK, Mem_A, splitMEM_B, X"00", Mem_OP, RE_A, RE_B, open, RE_OP);
	
	LC_REG: lc port map(RE_OP, lc_out);
	BC_R: banc_registre port map(CK, RST_REG, DI_B(3 downto 0), DI_C(3 downto 0), lc_out, RE_A(3 downto 0), muxRE_DATA, muxREG_QA, REG_QB);
	
	splitB: splitter port map(Mem_B, splitMEM_B, data_do);
	
	muxREG: multiplexeur generic map (6) port map(DI_OP, DI_B, muxREG_QA, muxREG_B);
	muxRE: multiplexeur generic map (4) port map(RE_OP, RE_B, data_di, muxRE_DATA);
	muxMem: multiplexeur generic map (4) port map(Mem_OP, Mem_A, Mem_B, data_a);

	UAL: ALU port map(EX_B, EX_C, EX_OP, muxALU_S, open);
	muxALU: multiplexeur generic map (5) port map(EX_OP, EX_B, muxALU_S, muxALU_B);
	LC_Mem: lc2 port map(Mem_OP, data_we);
	
	jmp_logik : JMP_LOGIC port map(DI_OP, muxREG_QA, jmp);
	splitJMP: splitter port map(DI_A, jmp_a, DI_A_split);

end Structural;

