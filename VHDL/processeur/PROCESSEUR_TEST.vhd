--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:42:49 05/27/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/PROCESSEUR_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: PROCESSEUR
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY PROCESSEUR_TEST IS
END PROCESSEUR_TEST;
 
ARCHITECTURE behavior OF PROCESSEUR_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PROCESSEUR
    PORT(
         CK : IN  std_logic;
			RST_REG: IN std_logic;
         ins_di : IN  std_logic_vector(31 downto 0);
         data_di : IN  std_logic_vector(7 downto 0);
         data_we : OUT  std_logic;
         data_a : OUT  std_logic_vector(7 downto 0);
         data_do : OUT  std_logic_vector(7 downto 0);
		   jmp: out std_logic;
		   jmp_a: out std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CK : std_logic := '0';
	signal RST_REG: std_logic := '0';
   signal ins_di : std_logic_vector(31 downto 0) := (others => '0');
   signal data_di : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal data_we : std_logic;
   signal data_a : std_logic_vector(7 downto 0);
   signal data_do : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace CK below with 
   -- appropriate port name 
 
   constant CK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PROCESSEUR PORT MAP (
          CK => CK,
			 RST_REG => RST_REG,
          ins_di => ins_di,
          data_di => data_di,
          data_we => data_we,
          data_a => data_a,
          data_do => data_do
        );

   -- Clock process definitions
   CK_process :process
   begin
		CK <= '0';
		wait for CK_period/2;
		CK <= '1';
		wait for CK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CK_period*10;

      RST_REG <= '1';
		ins_di <= X"F4000300" after 200 ns, X"00000000" after 210 ns,	-- AFC R0 3
					 X"F4010100" after 300 ns, X"00000000" after 310 ns,	-- AFC R1 1
					 X"91000001" after 400 ns, X"00000000" after 410 ns,	-- ADD R0 R0 R1 (R2 <- R0+R1)
					 X"36100000" after 500 ns, X"00000000" after 510 ns;	-- STORE @10 R2
		

      wait;
   end process;

END;
