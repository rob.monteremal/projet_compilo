----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:22:25 05/14/2019 
-- Design Name: 
-- Module Name:    banc_registre - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity banc_memoire is
    Port ( CK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  DATAIN : in  STD_LOGIC_VECTOR (7 downto 0);
           at : in  STD_LOGIC_VECTOR (7 downto 0);
           RW : in  STD_LOGIC;
           DATAOUT : out  STD_LOGIC_VECTOR (7 downto 0));
end banc_memoire;

architecture Behavioral of banc_memoire is
	type BENCH is array(0 to 255) of STD_LOGIC_VECTOR (7 downto 0);
	signal memory : BENCH;
	
begin
	process
	begin
		wait until rising_edge(CK);
		
		if (RST = '0') then
			loop_reset : for k in 0 to 255 loop
				memory(k) <= X"00";
			end loop loop_reset;
		end if;
		if (RW = '0') then
			memory(to_integer(unsigned(at))) <= DATAIN;
			else DATAOUT <= memory(to_integer(unsigned(at)));
		end if;
	end process;

end Behavioral;