--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:03:16 05/14/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/banc_memoire_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: banc_memoire
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY banc_memoire_TEST IS
END banc_memoire_TEST;
 
ARCHITECTURE behavior OF banc_memoire_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT banc_memoire
    PORT(
         CK : IN  std_logic;
         RST : IN  std_logic;
         DATAIN : IN  std_logic_vector(7 downto 0);
         at : IN  std_logic_vector(7 downto 0);
         RW : IN  std_logic;
         DATAOUT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CK : std_logic := '0';
   signal RST : std_logic := '0';
   signal DATAIN : std_logic_vector(7 downto 0) := (others => '0');
   signal at : std_logic_vector(7 downto 0) := (others => '0');
   signal RW : std_logic := '0';

 	--Outputs
   signal DATAOUT : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace CK below with 
   -- appropriate port name 
 
   constant CK_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: banc_memoire PORT MAP (
          CK => CK,
          RST => RST,
          DATAIN => DATAIN,
          at => at,
          RW => RW,
          DATAOUT => DATAOUT
        );

   -- Clock process definitions
   CK_process :process
   begin
		CK <= '0';
		wait for CK_period/2;
		CK <= '1';
		wait for CK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
		DATAIN <= X"01";
		RST <= '1', '0' after 504 ns, '1' after 640 ns;
		RW <= '0', '1' after 200 ns, '0' after 300 ns;
		at <= X"00";
      wait;
   end process;

END;
