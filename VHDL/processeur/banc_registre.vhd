----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:22:25 05/14/2019 
-- Design Name: 
-- Module Name:    banc_registre - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity banc_registre is
    Port ( CK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  atA : in  STD_LOGIC_VECTOR (3 downto 0);
           atB : in  STD_LOGIC_VECTOR (3 downto 0);
           W : in  STD_LOGIC;
           atW : in  STD_LOGIC_VECTOR (3 downto 0);
           DATA : in  STD_LOGIC_VECTOR (7 downto 0);
           QA : out  STD_LOGIC_VECTOR (7 downto 0);
           QB : out  STD_LOGIC_VECTOR (7 downto 0));
end banc_registre;

architecture Behavioral of banc_registre is
	type BENCH is array(0 to 15) of STD_LOGIC_VECTOR (7 downto 0);
	signal banc : BENCH;
	
begin
	process
	begin
		wait until rising_edge(CK);
		
		if (RST = '0') then
			loop_reset : for k in 0 to 15 loop
				banc(k) <= X"00";
			end loop loop_reset;
		end if;
		if (W = '1') then
			banc(to_integer(unsigned(atW))) <= DATA;
		end if;
	end process;
	
	QA <= banc(to_integer(unsigned(atA)));
	QB <= banc(to_integer(unsigned(atB)));

end Behavioral;