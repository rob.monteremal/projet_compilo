--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:41:14 05/14/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/banc_registre_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: banc_registre
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY banc_registre_TEST IS
END banc_registre_TEST;
 
ARCHITECTURE behavior OF banc_registre_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT banc_registre
    PORT(
         CK : IN  std_logic;
         RST : IN  std_logic;
         atA : IN  std_logic_vector(3 downto 0);
         atB : IN  std_logic_vector(3 downto 0);
         W : IN  std_logic;
         atW : IN  std_logic_vector(3 downto 0);
         DATA : IN  std_logic_vector(7 downto 0);
         QA : OUT  std_logic_vector(7 downto 0);
         QB : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CK : std_logic := '0';
   signal RST : std_logic := '0';
   signal atA : std_logic_vector(3 downto 0) := (others => '0');
   signal atB : std_logic_vector(3 downto 0) := (others => '0');
   signal W : std_logic := '0';
   signal atW : std_logic_vector(3 downto 0) := (others => '0');
   signal DATA : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal QA : std_logic_vector(7 downto 0);
   signal QB : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace CK below with 
   -- appropriate port name 
 
   constant CK_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: banc_registre PORT MAP (
          CK => CK,
          RST => RST,
          atA => atA,
          atB => atB,
          W => W,
          atW => atW,
          DATA => DATA,
          QA => QA,
          QB => QB
        );

   -- Clock process definitions
   CK_process :process
   begin
		CK <= '0';
		wait for CK_period/2;
		CK <= '1';
		wait for CK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
   
	DATA <= X"01";
	W <= '0', '1' after 225 ns, '0' after 400 ns;
	atW <= X"1";
	RST <= '1', '0' after 500 ns, '1' after 600 ns;
	atA <= X"0";
	atB <= X"1";
	
	wait;
   end process;

END;
