--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:46:51 05/20/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/multiplexeur_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: multiplexeur
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY multiplexeur_TEST IS
END multiplexeur_TEST;
 
ARCHITECTURE behavior OF multiplexeur_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT multiplexeur
	 GENERIC (pos: NATURAL);
    PORT(
         OP : IN  std_logic_vector(7 downto 0);
         B_in : IN  std_logic_vector(7 downto 0);
         QA : IN  std_logic_vector(7 downto 0);
         B_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal OP : std_logic_vector(7 downto 0) := (others => '0');
   signal B_in : std_logic_vector(7 downto 0) := (others => '0');
   signal QA : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal B_out : std_logic_vector(7 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: multiplexeur PORT MAP (
          OP => OP,
          B_in => B_in,
          QA => QA,
          B_out => B_out
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      -- wait for 100 ns;	


      -- insert stimulus here 
		OP <= X"00", X"01" after 100 ns, X"00" after 200 ns;
		B_in <= X"10";
		QA <= X"01";
		
      wait;
   end process;

END;
