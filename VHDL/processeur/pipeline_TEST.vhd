--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:59:18 05/14/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/pipeline_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: pipeline
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY pipeline_TEST IS
END pipeline_TEST;
 
ARCHITECTURE behavior OF pipeline_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT pipeline
    PORT(
         CK : IN  std_logic;
         A_in : IN  std_logic_vector(7 downto 0);
         B_in : IN  std_logic_vector(7 downto 0);
         C_in : IN  std_logic_vector(7 downto 0);
         OP_in : IN  std_logic_vector(3 downto 0);
         A_out : OUT  std_logic_vector(7 downto 0);
         B_out : OUT  std_logic_vector(7 downto 0);
         C_out : OUT  std_logic_vector(7 downto 0);
         OP_out : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CK : std_logic := '0';
   signal A_in : std_logic_vector(7 downto 0) := (others => '0');
   signal B_in : std_logic_vector(7 downto 0) := (others => '0');
   signal C_in : std_logic_vector(7 downto 0) := (others => '0');
   signal OP_in : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal A_out : std_logic_vector(7 downto 0);
   signal B_out : std_logic_vector(7 downto 0);
   signal C_out : std_logic_vector(7 downto 0);
   signal OP_out : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace CK below with 
   -- appropriate port name 
 
   constant CK_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pipeline PORT MAP (
          CK => CK,
          A_in => A_in,
          B_in => B_in,
          C_in => C_in,
          OP_in => OP_in,
          A_out => A_out,
          B_out => B_out,
          C_out => C_out,
          OP_out => OP_out
        );

   -- Clock process definitions
   CK_process :process
   begin
		CK <= '0';
		wait for CK_period/2;
		CK <= '1';
		wait for CK_period/2;
   end process;
 
	-- Stimulus process
   stim_proc: process
   begin

	A_in <= X"00", X"01" after 275 ns, X"02" after 867 ns;	

   wait;
   end process;
END;
