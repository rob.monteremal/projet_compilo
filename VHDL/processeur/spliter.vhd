----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:13:59 05/17/2019 
-- Design Name: 
-- Module Name:    splitter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity splitter is
    Port ( in_pipe : in  STD_LOGIC_VECTOR (7 downto 0);
           out_bcr : out  STD_LOGIC_VECTOR (7 downto 0);
           out_proc : out  STD_LOGIC_VECTOR (7 downto 0));
end splitter;

architecture Behavioral of splitter is

begin

out_bcr <= in_pipe;
out_proc <= in_pipe;

end Behavioral;

