--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:16:17 05/17/2019
-- Design Name:   
-- Module Name:   /home/trnguyen/Documents/Automate/projet_compilo/VHDL/processeur/splitter_TEST.vhd
-- Project Name:  processeur
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: splitter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY splitter_TEST IS
END splitter_TEST;
 
ARCHITECTURE behavior OF splitter_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT splitter
    PORT(
         in_pipe : IN  std_logic_vector(7 downto 0);
         out_bcr : OUT  std_logic_vector(7 downto 0);
         out_proc : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in_pipe : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal out_bcr : std_logic_vector(7 downto 0);
   signal out_proc : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: splitter PORT MAP (
          in_pipe => in_pipe,
          out_bcr => out_bcr,
          out_proc => out_proc
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;

      -- insert stimulus here 
		in_pipe <= X"00", X"01" after 100 ns;
		
      wait;
   end process;

END;
