################################################################################
#
# IMPORT
#
################################################################################
import sys
import getopt
import time

################################################################################
#
# CLASSES
#
################################################################################

################
# CPU
# @def Class used to simulate the behavior of the CPU make within ise in vhdl. 
################
class CPU():

	def __init__(self, sizeRegister=16, sizeRAM=256, sizeinstMemory=256, verbose=0, lstInst=None, autoExec=False, sleep=0):
		self.register = [0 for i in range(sizeRegister)]
		self.RAM = [0 for i in range(sizeRAM)]
		self.instMemory = [0 for i in range(sizeinstMemory)]
		self.pointer = 0
		self.verbose = verbose
		self.sleep = sleep

		if (lstInst != None):
			self.loadInstructions(lstInst)

		if (autoExec):
			self.exectute()

	##
	# loadInstructions(lst)
	# @def Method used to load the content of a list (containing all the lines of
	# a hexadecimal file) into the cpu's instruction memory. As the instruction
	# memory of the cpu is a list, each index of it contains one line of the file,
	# so one instruction. Note that an error is thrown if the number of lines of
	# the file exceed the instruction memory capacity, but does not interrupt the
	# program: all remaining lines of the file are simply ignored
	# @param lst, the list containing all the line of the hexadecimal file
	##
	def loadInstructions(self, lst):
		try:
			for i in range(len(lst)):
				self.instMemory[i] = lst[i]
		except IndexError:
			if (self.verbose > 0):
				print("WARNING ! The instruction memory is full : from instruction "+str(i-1)+", no instruction will be added in the memory")

	##
	# execute()
	# @def Method used to exectute all the instructions contained in the instruction
	# memory of the cpu. Depending of the verbose level selected by the user and the
	# return of the instructions, it also provide possible detailed logs about the 
	# behavior of the cpu and its interactions with the registers, the instruction
	# memory and the RAM.
	##
	def exectute(self):
		while ((self.pointer < len(self.instMemory)) and (self.instMemory[self.pointer] != 0)):
			r = self.executeInstruction(self.instMemory[self.pointer])
			if (self.verbose > 0 and self.instMemory[self.pointer] != "00000000"):
				s = "\n"
				if (self.verbose == 1 or self.verbose > 3):
					s += "[Pointer : "+str(self.pointer)+"]\n[Instruction : "+str(self.instMemory[self.pointer])+"]"
				b = ""
				if (self.verbose > 3):
					b += "\n"
				for i in range(min(self.verbose-1, 2)):
					b += r[i]
				s += b
				print(s)
				if (self.verbose > 4):
					self.readRegister()
				if (self.verbose > 5):
					time.sleep(self.sleep)
			self.pointer += 1

		print("--- END : state of the RAM ---")
		self.readRAM()

	##
	#
	##
	def readRegister(self):
		s = "["
		for i in range(len(self.register)-1):
			s += str(self.register[i])+"] ["
		s += str(self.register[-1])+"]"
		print("Registers ---\n"+s)

	def readRAM(self):
		s = ""
		for i in range(len(self.RAM)):
			if (self.RAM[i] != 0):
				s += "RAM "+str(i)+" = "+str(self.RAM[i])+"\n"
		print(s)
	##
	# executeInstruction(instruction)
	# @def Method used to execute one instruction.
	# @param instruction, the instruction in hexadecimal to be executed
	# @return a tuple containing logs of the cpu behavior when executing the instruction
	##
	def executeInstruction(self, instruction):
		OP, A, B, C = instruction[0:2], int(instruction[2:4], 16), int(instruction[4:6], 16), int(instruction[6:8], 16)

		if (OP == "00"):	# NOP
			return ("", "")

		if (OP == "f4"):	# AFC
			self.register[A] = B
			return ("[R"+str(A)+" = "+str(self.register[A])+"]", " - AFC "+str(B)+" in R"+str(A))
		
		if (OP == "91") : 		#ADD
			oldB, oldC = str(self.register[B]), str(self.register[C])
			self.register[A] = self.register[B] + self.register[C]
			return ("[R"+str(A)+" = "+str(self.register[A])+"]"," - ADD "+oldB+" and "+oldC+" in R"+str(A))

		if (OP == "92") :		#MUL
			oldB, oldC = str(self.register[B]), str(self.register[C])
			self.register[A] = self.register[B] * self.register[C]
			return ("[R"+str(A)+" = "+str(self.register[A])+"]", " - MUL "+oldB+" and "+oldC+" in R"+str(A))

		if (OP == "93") : 		#SUB
			oldB, oldC = str(self.register[B]), str(self.register[C])
			self.register[A] = self.register[B] - self.register[C]
			return ("[R"+str(A)+" = "+str(self.register[A])+"]", " - SUB "+oldB+" and "+oldC+" in R"+str(A))
		
		if (OP == "e5") : #LOAD
			self.register[A] = self.RAM[B]
			return ("[R"+str(A)+" = "+str(self.register[A])+"]", " - LOAD "+str(self.RAM[B])+" from RAM["+str(B)+"] in R"+str(A))

		if (OP == "36") : #STORE
			self.RAM[A] = self.register[B]
			return ("[RAM"+str(A)+" = "+str(self.RAM[A])+"]", "- STORE "+str(self.register[B])+" from R"+str(B)+" in RAM["+str(A)+"]")

		if (OP == "97") : #EQU
			if self.register[B] == self.register[C] : 
				self.register[A] = 1
			else :
				self.register[A] = 0
			return ("[R"+str(A)+" = "+str(self.register[A])+"] (1=True, 0=False)", " - EQU between R"+str(B)+" ("+str(self.register[B])+") and R"+str(C)+" ("+str(self.register[C])+"). The result is saved in R"+str(A))

		if (OP == "98") : #INF
			oldB, oldC = str(self.register[B]), str(self.register[C])
			if self.register[B] < self.register[C] :
				self.register[A] = 1 
			else :
				self.register[A] = 0
			return ("[R"+str(A)+" = "+str(self.register[A])+"] (1=True, 0=False)", " - INF between R"+str(B)+" ("+oldB+") and R"+str(C)+" ("+oldC+"). The result is saved in R"+str(A))

		if (OP == "99") : #INFE
			oldB, oldC = str(self.register[B]), str(self.register[C])
			if self.register[B] <= self.register[C] :
				self.register[A] = 1 
			else :
				self.register[A] = 0
			return ("[R"+str(A)+" = "+str(self.register[A])+"] (1=True, 0=False)", " - INFE between R"+str(B)+" ("+oldB+") and R"+str(C)+" ("+oldC+"). The result is saved in R"+str(A))

		if (OP == "9a") : #SUP
			if self.register[B] > self.register[C] :
				self.register[A] = 1 
			else :
				self.register[A] = 0
			return ("[R"+str(A)+" = "+str(self.register[A])+"] (1=True, 0=False)", " - SUP between R"+str(B)+" ("+str(self.register[B])+") and R"+str(C)+" ("+str(self.register[C])+"). The result is saved in R"+str(A))

		if (OP == "9b") : #SUPE
			if self.register[B] >= self.register[C] :
				self.register[A] = 1 
			else :
				self.register[A] = 0
			return ("[R"+str(A)+" = "+str(self.register[A])+"] (1=True, 0=False)", " - SUPE between R"+str(B)+" ("+str(self.register[B])+") and R"+str(C)+" ("+str(self.register[C])+"). The result is saved in R"+str(A))

		if (OP == "2c") : #JMP
			self.pointer = A-1
			return ("[Pointer = "+str(A)+"]", " - JMP to instruction "+str(self.pointer)+" {"+str(self.instMemory[self.pointer])+"}")

		if (OP == "2d") : #JMPC
			if self.register[B] == 0 :
				self.pointer = A-1
			return ("[Pointer = "+str(A)+"] (R"+str(B)+" = 0)", " - JMPC to instruction "+str(self.pointer)+" {"+str(self.instMemory[self.pointer])+"}")


################################################################################
#
# FUNCTIONS
#
################################################################################


##
# openFile(fileName)
# @def Function used to open a file and get its content
# @param fileName, the name of the file
# @return a list where each index contains one line of the file
##
def openFile(fileName):
	with open(fileName, 'r') as f:
		lst = f.readlines()
		for i in range(len(lst)):
			lst[i] = lst[i].replace("\n", "")
		return lst

##
# usage()
# @def Function used as the help for the program. Its called whether the user ask
# for the help using the [-h | --help] argument when calling the program, or if
# the user make a mistake when calling the program
##
def usage():
	print("\nUsage: python3 interpreter.py [-f | --file= <filename>]")
	print("optional arg :\n")
	print("\n\t[-s | --sleep <time>] where time is exprimed in second(s)")
	print("\t\tThis optional arg is only used if the verbose level is set to 6, otherwise it will be ignored\n")
	print("\t[-v | --verbose= <number>] where number need to be lower or equal to 5")
	print("\t\tnumber = 0 - No logging of executed code\n\t\tnumber = 1 - Precision about pointer position and next instruction readed\n\t\tnumber = 2 - Detailed output of the instruction readed\n\t\tnumber = 3 - Detailed output and execution behavior of the instruction readed\n\t\tnumber = 4 - Precision about pointer position and next instruction readed, as well as detailed output and exectution behavior of the instruction\n\t\tnumber = 5 - Precision about pointer position and next instruction readed, as well as detailed output and exectution behavior of the instruction and registers contents\n\t\tnumber 6 - All of the above, but use a timer to reduce the flow of logs: this level of verbose need to be used with the optional arg [-s | --sleep] in addition of  to be effective\n")


################################################################################
#
# MAIN
#
################################################################################

	


if __name__ == "__main__":
	f = None	# hex file name
	v = 0		# verbose level
	ae = False	# autoExec
	s = 0

	try:
		opts, args = getopt.getopt(sys.argv[1:], "h:f:v:s:", ["file=", "verbose=", "sleep="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in  opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt in ("-f", "--file"):
			f = arg
		elif opt in ("-v", "--verbose"):
			v = int(arg)
		elif opt in ("-s", "--sleep"):
			s = float(arg)

	if (v > 6):
		usage()
		sys.exit(2)
	else:
		c = CPU(lstInst=openFile(f), autoExec=True, verbose=v, sleep=s)
