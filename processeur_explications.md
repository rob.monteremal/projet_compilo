[V.1]

registre ----------> ALU ----------> meme data
20ns                 30ns            50ns      => 100 ns


[V.2] Avec pipeline => permet de paralléliser

[] --> registre ----[]-----> ALU ----[]----> meme data
       20ns                  30ns            50ns
       (50ns)     <--->      (50ns) <--->    (50ns)

Temps le plus long pour chaque execution

Exemple d'instruction : STORE 2 0

[] 2-> registre ----[]-----> ALU ----[]4---> meme data
[] 0--------------->[]4------------->[]
[]                  []0------------->[]0--->



BUT : faire chacun des composants 1 par 1, puis les interfacer pour faire le
processeur


ALU

process ...
begin
	b <- a;
	c <- b;
end;

a | b | c
--+---+--
1 | 2 | 3
--+---+--
1 | 1 | 2

A retenir : les opérations se font en parallèles (ici le b affecté à c est
l'ancien b, avant affectation de a)

S <- ('O'&A) + ('O'&B) when op = x"OO" else
     (A * B) when op = x"01"

opération : nombre de bits
8 + 8 -> 8
('O'&A) + ('O'&B)

8 * 8 -> 16

problème : on doit tronquer pour que le résultat tienne sur 8 bits
solution :

S_add <- ('O'&A) +  ...
S_mul <- ...
S <- S_add[7->0] when op = x"00" else
     S_mul[7->0] when op = x"01"
