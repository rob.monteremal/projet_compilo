%{
	#define debug_token 0
	#include "y.tab.h"

	void yyerror(const char*);
	int yylex(void);
%}

%%
"main" {if(debug_token){printf("tMAIN\n");} return tMAIN;}
"return" {if(debug_token){printf("tRETURN\n");} return tRETURN;}
"printf" {if(debug_token){printf("tPRINTF\n");} return tPRINTF;}
"const" {if(debug_token){printf("tCONST\n");} return tCONST;}
"int" {if(debug_token){printf("tINT\n");} return tINT;}
"float" {if(debug_token){printf("tFLOAT\n");} return tFLOAT;}
"double" {if(debug_token){printf("tDOUBLE\n");} return tDOUBLE;}
"for" {if(debug_token){printf("tFOR\n");} return tFOR;}
"while" {if(debug_token){printf("tWHILE\n");} return tWHILE;}
"do" {if(debug_token){printf("tDO\n");} return tDO;}
"if" {if(debug_token){printf("tIF\n");} return tIF;}
"else" {if(debug_token){printf("tELSE\n");} return tELSE;}
"switch" {if(debug_token){printf("tSWITCH\n");} return tSWITCH;}
"+" {if(debug_token){printf("tPLUS\n");} return tPLUS;}
"-" {if(debug_token){printf("tMINUS\n");} return tMINUS;}
"*" {if(debug_token){printf("tMULTI\n");} return tMULTI;}
"/" {if(debug_token){printf("tDIV\n");} return tDIV;}
"%" {if(debug_token){printf("tMODULO\n");} return tMODULO;}
"==" {if(debug_token){printf("tDOUBLEEQUAL\n");} return tDOUBLEEQUAL;}
"<=" {if(debug_token){printf("tINFOREQUAL\n");} return tINFOREQUAL;}
">=" {if(debug_token){printf("tSUPOREQUAL\n");} return tSUPOREQUAL;}
"<" {if(debug_token){printf("tINF\n");} return tINF;}
">" {if(debug_token){printf("tSUP\n");} return tSUP;}
"=" {if(debug_token){printf("tEQUAL\n");} return tEQUAL;}
"(" {if(debug_token){printf("tOP\n");} return tOP;}
")" {if(debug_token){printf("tCP\n");} return tCP;}
" "|["\t"]|["\n"] {;}
"\\n" {if(debug_token){printf("tRETURN\n");} return tRETURN;}
"," {if(debug_token){printf("tCOMMA\n");} return tCOMMA;}
";" {if(debug_token){printf("tSEMICOLON\n");} return tSEMICOLON;}
"{" {if(debug_token){printf("tOB\n");} return tOB;}
"}" {if(debug_token){printf("tCB\n");} return tCB;}
"[" {if(debug_token){printf("tOSQB\n");} return tOSQB;}
"]" {if(debug_token){printf("tCSQB\n");} return tCSQB;}
[a-zA-Z][a-zA-Z0-9_]* {if(debug_token){printf("tVAR: %s\n", yytext);} yylval.str = strdup(yytext); return tVAR;}
[0-9]+ {if(debug_token){printf("tINTEGER: %s\n", yytext);} yylval.nb = atoi(yytext); return tINTEGER;}

%%

void yyerror(const char* err)	{
    printf("Error: %s\n", err);
}
