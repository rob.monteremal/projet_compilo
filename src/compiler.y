%{
	#define debug_symbol_table 0
	#define debug_instruction_table 0
	#define debug_assembly 0
	#define debug 1
	#define output 0

	#include <string.h>
	#include <stdio.h>
	#include "../src/table.h"
	#include "../src/instructionTable.h"
%}

%union {
	char *str;
	int nb;
}

%token tINT tCONST tFLOAT tDOUBLE
%token <str>tVAR <nb>tINTEGER
%token tMAIN tPRINTF tRETURN
%token tFOR tWHILE tDO tIF tELSE tSWITCH
%token tPLUS tMINUS tMULTI tDIV tMODULO tEQUAL
%token tINF tSUP tDOUBLEEQUAL tINFOREQUAL tSUPOREQUAL
%token tOP tCP tCOMMA tSEMICOLON tOB tCB tOSQB tCSQB


%type <nb> Formula

%left tINF tSUP tDOUBLEEQUAL tINFOREQUAL tSUPOREQUAL
%right tEQUAL
%left tPLUS tMINUS
%left tMULTI tDIV tMODULO

%start S

%%
S :	{
		printf("Starting ...\n");
	}
	tINT tMAIN tOP tCP tOB	{
		gainDepth();
	}
	Body Return tCB	{
		//loseDepth();
		printf("\nFile [OK]\n");
	};

Body : Lines;
Lines : Line | Line Lines;
Line : Declaration tSEMICOLON 
	| Affectation tSEMICOLON 
	| If tOB  {
			if(debug_assembly)	{
				printf("~JMPC X, R0~\n");
			}
			// a temporary JMPC instruction is added with -1 as its jumping address
			addInstruction("JMPC", -1, 0, -2, EMPTY, REGISTER, EMPTY);
			gainDepth();
		}	Lines tCB	{
//			addInstruction("NOP", 0, 0, 0, EMPTY, EMPTY, EMPTY);
			loseDepth();
		} MaybeElse
	| While {
//			addInstruction("NOP", 0, 0, 0, EMPTY, EMPTY, EMPTY);
		};

Type : tINT	{
			YTYPE = strdup("int");
		} 
	| tFLOAT	{
			YTYPE = strdup("float");
		}
	| tDOUBLE	{
			YTYPE = strdup("double");
		};

Declaration	: MaybeConst Type tVAR
	{
		delTABtmp();
		addTAB($3, YTYPE, YCONST, 0);
		if(debug_symbol_table){printTAB();}
		if(debug_instruction_table){printIT();}
	}
	| MaybeConst Type tVAR tEQUAL Formula
	{ 
		delTABtmp();
		addTAB($3, YTYPE, YCONST, 0);
		if(debug_assembly)	{
			printf("LOAD R0, 0x%d\n", $5);
			printf("STORE 0x%d, R0\n", searchTAB($3));
		}
		addInstruction("LOAD", 0, $5, -2, REGISTER, ADDRESS, EMPTY); 
		addInstruction("STORE", searchTAB($3), 0, -2, ADDRESS, REGISTER, EMPTY); 
		if(debug_symbol_table){printTAB();}
		if(debug_instruction_table){printIT();}
	};

Affectation : tVAR tEQUAL Formula
	{ 
		delTABtmp();
		if(debug_assembly)	{
			printf("LOAD R0, 0x%d\n", $3);
			printf("STORE 0x%d, R0\n", searchTAB($1));
		}
		addInstruction("LOAD", 0, $3, -2, REGISTER, ADDRESS, EMPTY); 
		addInstruction("STORE", searchTAB($1), 0, -2, ADDRESS, REGISTER, EMPTY); 
		//if(debug_symbol_table){printTAB();}
		if(debug_instruction_table){printIT();}
	};

MaybeConst : tCONST {YCONST = 1;} | {YCONST = 0;};

Formula	: Formula tPLUS Formula
			{
				if(debug_assembly)	{
					printf("LOAD R0, 0x%d\n", $1);
					printf("LOAD R1, 0x%d\n", $3);
					printf("ADD R0, R0, R1\n");
					printf("STORE 0x%d, R0\n", $1);
				}
				addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("ADD", 0, 0, 1, REGISTER, REGISTER, REGISTER);
				addInstruction("STORE", $1, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = $1;
			}
		| Formula tMINUS Formula
			{
				if(debug_assembly)	{
					printf("LOAD R0, 0x%d\n", $1);
					printf("LOAD R1, 0x%d\n", $3);
					printf("SUB R0, R0, R1\n");
					printf("STORE 0x%d, R0\n", $1);
				}
				addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("SUB", 0, 0, 1, REGISTER, REGISTER, REGISTER);
				addInstruction("STORE", $1, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = $1;
			}
		| Formula tMULTI Formula
			{
				if(debug_assembly)	{
					printf("LOAD R0, 0x%d\n", $1);
					printf("LOAD R1, 0x%d\n", $3);
					printf("MUL R0, R0, R1\n");
					printf("STORE 0x%d, R0\n", $1);
				}
				addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("MUL", 0, 0, 1, REGISTER, REGISTER, REGISTER);
				addInstruction("STORE", $1, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = $1;
			}
		| Formula tDIV Formula
			{
				if(debug_assembly)	{
					printf("LOAD R0, 0x%d\n", $1);
					printf("LOAD R1, 0x%d\n", $3);
					printf("DIV R0, R0, R1\n");
					printf("STORE 0x%d, R0\n", $1);
				}
				addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("DIV", 0, 0, 1, REGISTER, REGISTER, REGISTER);
				addInstruction("STORE", $1, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = $1;
			}
		| tINTEGER
			{
				int addr = addTABtmp();
				if(debug_assembly)	{
					printf("AFC R0, %d\n", $1);
					printf("STORE 0x%d, R0\n", addr);
				}
				addInstruction("AFC", 0, $1, -2, REGISTER, VALUE, EMPTY);
				addInstruction("STORE", addr, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = addr;
			}
		| tVAR
			{
				int addr = addTABtmp();
				if(debug_assembly)	{
					printf("LOAD R0, 0x%d\n", searchTAB($1));
					printf("STORE 0x%d, R0\n", addr);
				}
				addInstruction("LOAD", 0, searchTAB($1), -2, REGISTER, ADDRESS, EMPTY);
				addInstruction("STORE", addr, 0, -2, ADDRESS, REGISTER, EMPTY);
				$$ = addr;
			}
		;

// Comparisons between two formulas (<, >, <=, >=, ==)
Comp : Formula tINF Formula
		{
			if(debug_assembly)	{
				printf("LOAD R0, 0x%d\n", $1);
				printf("LOAD R1, 0x%d\n", $3);
				printf("INF R0, R0, R1\n");
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("INF", 0, 0, 1, REGISTER, REGISTER, REGISTER);
		}
	| Formula tSUP Formula 
		{
			if(debug_assembly)	{
				printf("LOAD R0, 0x%d\n", $1);
				printf("LOAD R1, 0x%d\n", $3);
				printf("SUP R0, R0, R1\n");
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("SUP", 0, 0, 1, REGISTER, REGISTER, REGISTER);
		}
	| Formula tINFOREQUAL Formula 
		{
			if(debug_assembly)	{
				printf("LOAD R0, 0x%d\n", $1);
				printf("LOAD R1, 0x%d\n", $3);
				printf("INFE R0, R0, R1\n");
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("INFE", 0, 0, 1, REGISTER, REGISTER, REGISTER);
		}
	| Formula tSUPOREQUAL Formula 
		{
			if(debug_assembly)	{
				printf("LOAD R0, 0x%d\n", $1);
				printf("LOAD R1, 0x%d\n", $3);
				printf("SUPE R0, R0, R1\n");
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("SUPE", 0, 0, 1, REGISTER, REGISTER, REGISTER);
		}
	| Formula tDOUBLEEQUAL Formula 
		{
			if(debug_assembly)	{
				printf("LOAD R0, 0x%d\n", $1);
				printf("LOAD R1, 0x%d\n", $3);
				printf("EQU R0, R0, R1\n");
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("LOAD", 1, $3, -2, REGISTER, ADDRESS, EMPTY);
			addInstruction("EQU", 0, 0, 1, REGISTER, REGISTER, REGISTER);
		}
	|  Formula 
		{
			if(debug_assembly)	{
				printf("LOAD R0, %d\n", $1);
			}
			addInstruction("LOAD", 0, $1, -2, REGISTER, ADDRESS, EMPTY);
		}
	;

// If ( Comp )
If : tIF tOP Comp tCP;
	
// Used to seperate a If alone and a If followed by a Else
MaybeElse : 
	tELSE tOB {
			if(debug_assembly)	{
				printf("~JMP X~\n");
			}
			// a temporary JMP instruction (for the ELSE part) is added with -1 as its jumping address
			addInstruction("JMP", -1, -2, -2, EMPTY, EMPTY, EMPTY);
			// the temporary JMPC instruction (for the IF part) is now updated with the correct jumping address
			int addr = updateLatestJumpInstruction("JMPC");
			if(debug_assembly)	{
				printf("~JMPC 0x%d, R0~\n", addr);
			}
			gainDepth();
		} Lines tCB {
			// the temporary JMPC instruction (for the ELSE part) is now updated with the correct jumping address
			int addr = updateLatestJumpInstruction("JMP");
			if(debug_assembly)	{
				printf("~JMP 0x%d~\n", addr);
			}
//			addInstruction("NOP", -2, -2, -2, EMPTY, EMPTY, EMPTY);
			loseDepth();
		}

	|	{	// the temporary JMPC instruction is now updated with the correct jumping address
			int addr = updateLatestJumpInstruction("JMPC");
			if(debug_assembly)	{
				printf("~JMPC 0x%d, R0~\n", addr);
			}
		};

While :
	tWHILE {
			int addr = currentAddress();
			if(debug_assembly) {
				printf("~TMP 0x%d~\n", addr);
			}
			// a special TMP (of size 0) instruction is added to save the address of the start of the while loop
			addInstruction("TMP", addr, -2, -2, ADDRESS, EMPTY, EMPTY);
		} tOP Comp tCP tOB {
			if(debug_assembly)	{
				printf("~JMPC X, R0~\n");
			}
			// a temporary JMPC instruction is added with -1 as its jumping address
			addInstruction("JMPC", -1, 0, -2, EMPTY, REGISTER, EMPTY);
			gainDepth();
		} Lines tCB {
			int addr = deleteLatestTMPInstruction();
			if(debug_assembly)	{
				printf("JMP 0x%d\n", addr);
			}
			// the jumping address is retrieved from the TMP instruction
			addInstruction("JMP", addr, -2, -2, ADDRESS, EMPTY, EMPTY);
			// the temporary JMPC instruction is now updated with the correct jumping address
			int addr2 = updateLatestJumpInstruction("JMPC");
			if(debug_assembly)	{
				printf("~JMPC 0x%d, R0~\n", addr2);
			}
			loseDepth();
		};
	
Return : tRETURN tINTEGER tSEMICOLON;

%%


int main()
{
	initTAB();
	initIT();
	yyparse();
	// a NOP is added at the very end of the file
	addInstruction("NOP", 0, 0, 0, EMPTY, EMPTY, EMPTY);

	if(debug_symbol_table || debug){printTAB();}
	if(debug_instruction_table || debug){printIT();}

	char* outputFileName = "compilerOutput.hex";
	writeInstructionsToFile(outputFileName, output);

	return 0;
}
