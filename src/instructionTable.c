/*
 * https://fr.wikipedia.org/wiki/Table_des_symboles
 * https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c/19868-les-piles-et-les-files
 * VHDL instructions table: https://docs.google.com/spreadsheets/d/1dTCObvSyGPIOfv_O5LyZFMx6-FhijdjYly-2A45o-Vc/edit?usp=sharing
*/

#define MAX_SYMBOL_SIZE 128
#define PC_OFFSET 0

/**************** INCLUDES ****************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "instructionTable.h"

/******************************************/

// Instruction table and reverse instruction table
instructionTable* IT;
instructionTable* IT_rev;

//translation table when writing to file (translates an address to its real value (with NOP taken into account)
int translationNOP[256];

/*************** FONCTIONS ****************/
/*
 * obligatoire vu que TAB est une variable globale (?)
*/
void initIT(void)
{
	IT = malloc(sizeof(instructionTable));
	IT->first = NULL;
	IT->size = PC_OFFSET;
}

void initIT_rev(void)	{
	IT_rev = malloc(sizeof(instructionTable));
	IT_rev->first = NULL;
	IT_rev->size = PC_OFFSET;
}

int currentAddress(void)	{
	return IT->size;
}

/*
 * prints the entirety of the instruction table
 */
void printIT(void)
{
	printf("IT (%d): name | r0 | r1 | r2\n", IT->size);
	instruction *i = IT->first;
	while (i != NULL)	{
		printf("0x%02x: %s | ", i->address, i->name);

		if(i->type0 == REGISTER)	{
			printf("r%d | ", i->arg0);
		} else if (i->type0 == VALUE) {
			printf("%d | ", i->arg0);
		} else if (i->type0 == ADDRESS) {
			printf("0x%02x | ", i->arg0);
		} else if (i->type0 == EMPTY) {
			printf("X | ");
		}
		
		if(i->type1 == REGISTER)	{
			printf("r%d | ", i->arg1);
		} else if (i->type1 == VALUE) {
			printf("%d | ", i->arg1);
		} else if (i->type1 == ADDRESS) {
			printf("0x%02x | ", i->arg1);
		} else if (i->type1 == EMPTY) {
			printf("X | ");
		}
		
		if(i->type2 == REGISTER)	{
			printf("r%d\n", i->arg2);
		} else if (i->type2 == VALUE) {
			printf("%d\n", i->arg2);
		} else if (i->type2 == ADDRESS) {
			printf("0x%02x\n", i->arg2);
		} else if (i->type2 == EMPTY) {
			printf("X\n");
		}

		i = i->next;
	}
	printf("\n");
}

/*
 * Removes the latest TMP instruction
 * returns the address stored in the first parameter
 */
int deleteLatestTMPInstruction(void)	{
	// if the stack is empty
	if(IT->first == NULL)	{
		return -1;
	}
	
	instruction *i = IT->first;
	instruction *previous /*= malloc(sizeof(instruction))*/;
	// walks the stack
	while(i->next != NULL)	{
		// if we find a TMP instruction, it gets removed
		if(!strcmp(i->name, "TMP"))	{
			previous->next = i->next;
			int addr = i->arg0;
			free(i);
			return addr;
		}
		previous = i;
		i = i->next;
	}
	
	return 0;
}

/*
 * Updates the latest *unresolved* JMP or JMPC instruction with the actual address
 * an unresolved JMP or JMPC instruction has EMPTY as its destination address
 */
int updateLatestJumpInstruction(char* name)	{
	int addr = currentAddress();
	instruction *i = IT->first;
	//walks the list
	while (i != NULL)	{
		// is a JMPC and is unresolved
		if (!strcmp(i->name, name) && (i->type0 == EMPTY)) {
			// JMPC set to current address
			i->type0 = ADDRESS;
			i->arg0 = addr;
			return addr;
		}
		i = i->next;
	}
	printf("Error: no unresolved %s instruction found!\n", name);
	
	return 0;
}

/*
 * Saves an instruction
 */
void addInstruction(char *name, int arg0, int arg1, int arg2, int type0, int type1, int type2) 	// faudra gerer le type aussi, et du coup le decalage adresse
{
	instruction *i = malloc(sizeof(instruction));
	i->name = strdup(name);
	i->arg0 = arg0;
	i->arg1 = arg1;
	i->arg2 = arg2;
	i->type0 = type0;
	i->type1 = type1;
	i->type2 = type2;

	pushInstruction(i);
}

/*
 * pushes an instruction onto the instruction table (stack)
 */
void pushInstruction(instruction* i)	{
	i->address = currentAddress();
	// If the table isn't empty
	if (IT->first != NULL) {
		i->next = IT->first;
		IT->first = i;
	}
	else {
		IT->first = i;
	}
	// if the instruction is TMP, the table size/addresses aren't incremented
	// because a TMP instruction will get deleted later on (used in WHILE loops)
	if(strcmp(i->name, "TMP"))	{
		IT->size += 1;
	}
}

/*
 * Reverses the linked list to be printed in the correct way in the output file
 */
void reverseInstructionTable(void)	{
	instruction *i = IT->first;
	instruction *next = NULL;
	instruction *prev = NULL;

	while(i != NULL)	{
		next = i->next;
		i->next = prev;
		prev = i;
		i = next;
//		printf("%s\n", i->name);
	}
	IT->first = prev;
//	printf("End of reverse\n");
//	printIT();
}

/*
 * Converts an instruction name to its HEX code (cf VHDL instructions table)
 */
int instructionDecoder(char* name)	{
	if(!strcmp("NOP", name))
		return 0x0;
	else if(!strcmp("ADD", name))
		return 0x91;
	else if(!strcmp("MUL", name))
		return 0x92;
	else if(!strcmp("SUB", name))
		return 0x93;
	else if(!strcmp("EQU", name))
		return 0x97;
	else if(!strcmp("INF", name))
		return 0x98;
	else if(!strcmp("INFE", name))
		return 0x99;
	else if(!strcmp("SUP", name))
		return 0x9A;
	else if(!strcmp("SUPE", name))
		return 0x9B;
	else if(!strcmp("AFC", name))
		return 0xF4;
	else if(!strcmp("LOAD", name))
		return 0xE5;
	else if(!strcmp("STORE", name))
		return 0x36;
	else if(!strcmp("JMP", name))
		return 0x2C;
	else if(!strcmp("JMPC", name))
		return 0x2D;
	else {
		printf("Error: unsupported instruction!\n");
		return 0;
	}
}

/*
 * inserts NOP codes with 3 different modes :
 * 0 - no NOP insertion
 * 1 - full NOP insertion (4 between each instruction)
 * 2 - smart NOP insertion (cf gestion des aléas)
 */
 void insertNOP(int mode)	{
	instruction *i = IT->first;
	instruction *j;
	int k, n;
	// keeps track of the latest write in each register
	int regRW[16];
	for (k=0 ; k<16 ; k++)	{
		regRW[k] = 0;
	}

	if(mode == 0)	{
		printf("Mode selected: No NOP insertion\n");
	} else if (mode == 1)	{
		printf("Mode selected: Full NOP insertion\n");
		while(i != NULL)	{
			// 4 NOPs are inserted
			for (k = 0; k<4 ; k++)	{
				sub_insertNOP(i);
				i = i->next;
			}
			i = i->next;
		}
	} 
	else if (mode == 2)	{
		printf("Mode selected: Smart NOP insertion\n");
		while(i->next != NULL)	{
			n = 0;
			// i is an ALU instruction or AFC
			if (!strcmp(i->name, "ADD") || 
			!strcmp(i->name, "MUL") || 
			!strcmp(i->name, "SUB") || 
			!strcmp(i->name, "AFC") || 
			!strcmp(i->name, "EQU") || 
			!strcmp(i->name, "INF") || 
			!strcmp(i->name, "INFE") || 
			!strcmp(i->name, "SUP") || 
			!strcmp(i->name, "SUPE"))	{
				regRW[i->arg0] = 3;
			} else if (!strcmp(i->name, "LOAD"))	{
				regRW[i->arg0] = 3;
			}
			
			j = i->next;				
			// j is an ALU instruction
			if (!strcmp(j->name, "ADD") || 
			!strcmp(j->name, "MUL") || 
			!strcmp(j->name, "SUB") || 
			!strcmp(j->name, "EQU") || 
			!strcmp(j->name, "INF") || 
			!strcmp(j->name, "INFE") || 
			!strcmp(j->name, "SUP") || 
			!strcmp(j->name, "SUPE"))	{
				// Max between arg1 and arg2
				if (regRW[j->arg1] >= regRW[j->arg2])	{
					n = regRW[j->arg1];
				} else {
					n = regRW[j->arg2];
				}
//				printf("ALU, adding %d NOP\n", n);
			} else if (!strcmp(j->name, "JMP") || !strcmp(j->name, "JMPC")) {
				n = 3;
//				printf("JUMP, adding %d NOP\n", n);
//				printf("===== JUMP was %x now %x\n", j->address, translationNOP[j->address]); 
			} else if (!strcmp(j->name, "STORE"))	{
				n = regRW[j->arg1];
//				printf("ALU/STORE, adding %d NOP\n", n);
			}
			// inserts n NOP
			while (n>0)	{
				sub_insertNOP(i);
				i = i->next;
				n--;
			}
			
			// every iteration, number of necessary NOP is decreased
			for (k=0 ; k<16 ; k++)	{
				if (regRW[k] > 0)	{
					regRW[k]--;
				}
			}
			
			i = j;
		}
	} else {
		printf("Mode selected: Error unrecognized mode\n");
	}
}

void sub_insertNOP(instruction* i)	{
	//keeps track of how many NOP instruction have been inserted (to shift jumping addresses)
	static int nopCount = 0;
	translationNOP[i->address] = i->address+nopCount;
	nopCount++;
//	printf("%x is now %x\n", i->address, i->address+nopCount);
	
	// Creates a NOP instruction
	instruction* nop = malloc(sizeof(instruction));
	nop->name = "NOP";
	nop->address = i->address+1;
	nop->type0 = EMPTY;
	nop->type1 = EMPTY;
	nop->type2 = EMPTY;
	
	// it gets inserted into the list
	nop->next = i->next;
	i->next = nop;
}
/*
 * writes a SINGLE instruction to a file
 */
void writeInstructionToFile(FILE* f, instruction* i, int mode)	{
	//Writes instruction code
	fprintf(f, "%02x", instructionDecoder(i->name));
	//First argument
	if(i->type0 == EMPTY)	{
		fprintf(f, "00");
	} else	{
		// JMP or JMPC instruction get their jumping addresses shifted (NOP insertion)
		if (!strcmp(i->name, "JMP") || !strcmp(i->name, "JMPC") && mode != 0) {
			fprintf(f, "%02x", translationNOP[i->arg0]);
			printf("%s shifted from %x to %x\n", i->name, i->arg0, translationNOP[i->arg0]);
		} else {
			fprintf(f, "%02x", i->arg0);
		}
	}
	//First argument
	if(i->type1 == EMPTY)	{
		fprintf(f, "00");
	} else	{
		fprintf(f, "%02x", i->arg1);
	}
	//Third argument
	if(i->type2 == EMPTY)	{
		fprintf(f, "00");
	} else	{
		fprintf(f, "%02x", i->arg2);
	}

	fprintf(f, "\n");
}

/*
 * writes the full instruction table to a file
 */
int writeInstructionsToFile(char* path, int mode)	{
	reverseInstructionTable();
	int j;
	for (j=0 ; j<256 ; j++)	{
		translationNOP[j] = 255;
	}
	insertNOP(mode);

	//printIT();

	FILE* f = fopen(path, "w+");
	if (f == NULL)	{
		printf("Error opening %s", path);
		return -1;
	}
	
	instruction *i = IT->first;
	while(i != NULL)	{
		writeInstructionToFile(f, i, mode);
		i = i->next;
	}
	
	fclose(f);
	return 0;
}
/******************************************/
