#ifndef TABLE_INSTRUCTION_H
#define TABLE_INSTRUCTION_H

/*
 * Structure describing an instruction
 */
typedef struct instruction {
	char *name;
	int arg0;
	int arg1;
	int arg2;
	int type0; //type of 1st arg
	int type1; //type of 2nd arg
	int type2; //type of 3rd arg
	int address;
	struct instruction* next;
} instruction;

typedef enum {REGISTER, ADDRESS, VALUE, EMPTY} R_TYPE;
/*
 * Structure describing the symbol table
 * FILO (stack) structure via linked list
 */
typedef struct {
	instruction *first;
	int size;	//equivalent to PC
} instructionTable;

void initIT(void);
void printIT(void);
int currentAddress(void);
int updateLatestJumpInstruction(char* name);
int deleteLatestTMPInstruction(void);
void addInstruction(char* name, int arg0, int arg1, int arg2, int type0, int type1, int type2);
void pushInstruction(instruction* s);
int writeInstructionsToFile(char* path, int mode);
void insertNOP(int mode);
void sub_insertNOP(instruction* i);

#endif
