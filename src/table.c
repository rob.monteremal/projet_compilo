/*
 * https://fr.wikipedia.org/wiki/Table_des_symboles
 * https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c/19868-les-piles-et-les-files
*/

#define MAX_SYMBOL_SIZE 128
#define MEMORY_INIT 0
#define TEST_TESTOKOK 1

/**************** INCLUDES ****************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "table.h"

/******************************************/

// tab global (peut etre a foutre dans le .h avec les struct)
tab *TAB;	

/******************************************/

char *YTYPE = "";
int YCONST = 0;

/*************** FONCTIONS ****************/
/*
 * obligatoire vu que TAB est une variable globale (?)
*/
void initTAB() {
	TAB = malloc(sizeof(tab));
	TAB->first = NULL;
	TAB->size = 0;
	TAB->depth = 0;
	TAB->mem_adress = MEMORY_INIT;
}

/*
 * prints the entirety of the symbol table
 */
void printTAB() {
	printf("TAB: name | type | depth | is_const | is_init | mem\n");
	symbol *s = TAB->first;
	while (s != NULL)
	{
		printf("%s | %s | %d | %d | %d | %d | %d\n", s->name, s->type, s->depth, s->is_const, s->is_init, s->memory_emp, s->size);
		s = s->next;
	}
	printf("\n");
}

/*
 * Checks whether the symbol is already in the table
 * returns 0 if it isn't
 * returns the memory address of the symbol if it is
 */
int searchTAB(char *name) {
	symbol *s = TAB->first;
	//walks the list
	while (s != NULL)	{
		//printf("Search: %s\n", s->name);
		// if the symbol has the same name & depth
		if (!strcmp(s->name, name) && (s->depth <= TAB->depth)) {
			return s->memory_emp;	// returns memory location
		}
		s = s->next;
	}
	return 0;
}

int addTABtmp(void) {
	symbol *s = malloc(sizeof(symbol));
	s->name = " ";
	s->type = NULL;
	s->depth = TAB->depth;
	s->is_const = 1;
	s->is_init = 1;

	s->size = TEST_TESTOKOK;
	
	TAB->mem_adress += TEST_TESTOKOK;
	s->memory_emp = TAB->mem_adress;

	pushSymbol(s);

	return s->memory_emp;
}

/*
 * ajout d'un symbole dans TAB
 * Cela n'est possible que si le symbole ne s'y trouve pas deja (resultat de searchTAB)
 * Faudra check d autres trucs genre la declaration dans le if ou des trucs comme ça
 * -> a voir avec le prof (pas compris la derniere fois)
 */
void addTAB(char *name, char *type, int isConst, int isInit) 	// faudra gerer le type aussi, et du coup le decalage adresse
{
	if (!searchTAB(name)) {
		symbol *s = malloc(sizeof(symbol));
		s->name = strdup(name);
		s->type = strdup(type);
		s->depth = TAB->depth;
		s->is_const = isConst;
		s->is_init = isInit;

		/*
		if(strcmp(type, "int") == 0)	{
			s->size = sizeof(int);
		} else if(strcmp(type, "double") == 0)	{
			s->size = sizeof(double);
		} else if(strcmp(type, "float") == 0)	{
			s->size = sizeof(float);
		} else if(strcmp(type, "char") == 0)	{
			s->size = sizeof(char);
		} else {
			printf("Error: Unknown type %s\n", type);
			s->size += sizeof(double);
		}
		*/
		
		TAB->mem_adress += s->size = TEST_TESTOKOK;
		s->memory_emp = TAB->mem_adress;

		pushSymbol(s);
	}
}

/*
 * pushes a symbol onto the symbol table (stack)
 */
void pushSymbol(symbol* s)	{
	// If the table isn't empty
	if (TAB->first != NULL) {
		s->next = TAB->first;
		TAB->first = s;
	}
	else {
		TAB->first = s;
	}
	TAB->size += 1;
}


/*
 * pops the top of the symbol table (stack)
 * returns the element
 * returns NULL if the table is empty
 */
symbol* popSymbol()	{
	symbol *s = malloc(sizeof(symbol));
	// If the table isn't empty
	if(TAB->first != NULL)	{
		s = TAB->first;
		TAB->mem_adress -= s->size;	//decreases the memory adress (free)
		TAB->first = s->next;
		TAB->size -= 1;
	}

	return s;
}

/*
 * deletes the latest temp variables
 * returns its location in memoroy
 */
int delTABtmp(void)	{
	// bad practice I know :(
	if(TAB->first == NULL)	{
		return 0;
	}

	symbol *s = malloc(sizeof(symbol));
	s = TAB->first;
	// variable with " " as name are empty
	while(s->next != NULL)	{
		if(!strcmp(s->name, " "))	{
			TAB->size -= 1;
			TAB->mem_adress -= s->size;
			TAB->first = s->next;
		}
		s = s->next;
	}

	// please don't judge me I know it's bad
	if(!strcmp(s->name, " "))	{
		TAB->size -= 1;
		TAB->mem_adress -= s->size;
		TAB->first = s->next;
	}

	return 1;
}

void gainDepth(void) {
	TAB->depth += 1;
}

/*
 * removes one layer of depth
 * every variable declared within this layer are deleted
*/
void loseDepth(void) {
	TAB->depth -= 1;
	symbol *s = popSymbol();
	// removes symbols until the table is empty or it gets to the right depth
	while(s->depth > TAB->depth && s != NULL)	{
		//printf("Name: %s, depth: %d", s->name, s->depth);
		s = popSymbol();
	}
	// last symbol is added back (first one with the right depth)
	addTAB(s->name, s->type, s->is_init, s->is_const);
}

/******************************************/
