#ifndef TABLE_H
#define TABLE_H

/*
 * Structure describing a symbol
 */
typedef struct symbol {
	char *name;
	char *type;
	int size;		// memory space taken by symbol
	int depth;
	int is_const;	// booleen
	int is_init;	// booleen
	int memory_emp;	// DECALAGE par rapport a l'adresse de depart
	struct symbol* next;
} symbol;

/*
 * global variables
*/
extern char *YTYPE;
extern int YCONST;
/*
 * Structure describing the symbol table
 * FILO (stack) structure via linked list
 */
typedef struct {
	symbol *first;
	int size;
	int depth;	// la profondeur ACTUELLE
	int mem_adress; // adress of the most recent variable
} tab;

void initTAB();
void printTAB();
int searchTAB(char* name);
void addTAB(char* name, char* type, int isConst, int isInit);
void pushSymbol(symbol* s);
symbol* popSymbol();
void removeSymbol();
void gainDepth();
void loseDepth();
int addTABtmp(void);
int delTABtmp(void);

#endif
